﻿using Abp.Application.Services;
using Contact.Contact.Dto;
using Contact.EntityFrameworkCore.Repositories.Contact;
using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Repositories;
using Contact.Models;

namespace Contact.Contact
{
    public class ContactAppService : AsyncCrudAppService<Models.Contact, ContactOutput>
    {
        public ContactAppService(IContactRepository repository) : base(repository)
        {
        }
    }
}
