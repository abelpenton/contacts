﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contact.Contact.Dto
{
    [AutoMapFrom(typeof(Models.Contact))]
    public class ContactOutput: EntityDto
    {
        public string First_Name { get; set; }

        public string Last_Name { get; set; }

        public string Profession { get; set; }
    }
}
