﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contact.Contact.Dto
{
    public class ContactDtoProfile: Profile
    {
        public ContactDtoProfile()
        {
            CreateMap<ContactOutput, Models.Contact>();
        }
    }
}
