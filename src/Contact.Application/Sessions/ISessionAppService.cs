﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Contact.Sessions.Dto;

namespace Contact.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
