using System.ComponentModel.DataAnnotations;

namespace Contact.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}