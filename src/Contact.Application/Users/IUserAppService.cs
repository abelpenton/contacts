using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Contact.Roles.Dto;
using Contact.Users.Dto;

namespace Contact.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
    }
}
