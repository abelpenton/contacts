﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Contact.Authorization;

namespace Contact
{
    [DependsOn(
        typeof(ContactCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class ContactApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<ContactAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(ContactApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
