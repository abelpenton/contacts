﻿using System.Threading.Tasks;
using Contact.Configuration.Dto;

namespace Contact.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
