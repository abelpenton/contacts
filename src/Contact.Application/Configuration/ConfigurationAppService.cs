﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using Contact.Configuration.Dto;

namespace Contact.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : ContactAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
