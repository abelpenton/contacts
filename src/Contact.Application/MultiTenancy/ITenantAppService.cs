﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Contact.MultiTenancy.Dto;

namespace Contact.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
