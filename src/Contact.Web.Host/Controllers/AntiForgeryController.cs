using Microsoft.AspNetCore.Antiforgery;
using Contact.Controllers;

namespace Contact.Web.Host.Controllers
{
    public class AntiForgeryController : ContactControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
