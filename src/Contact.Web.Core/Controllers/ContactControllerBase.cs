using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace Contact.Controllers
{
    public abstract class ContactControllerBase: AbpController
    {
        protected ContactControllerBase()
        {
            LocalizationSourceName = ContactConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
