﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using Contact.Authorization.Roles;
using Contact.Authorization.Users;
using Contact.MultiTenancy;

namespace Contact.EntityFrameworkCore
{
    public class ContactDbContext : AbpZeroDbContext<Tenant, Role, User, ContactDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public ContactDbContext(DbContextOptions<ContactDbContext> options)
            : base(options)
        {
        }

        public DbSet<Contact.Models.Contact> Contacts { get; set; }
    }
}
