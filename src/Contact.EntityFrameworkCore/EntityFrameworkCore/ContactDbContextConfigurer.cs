using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace Contact.EntityFrameworkCore
{
    public static class ContactDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<ContactDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<ContactDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
