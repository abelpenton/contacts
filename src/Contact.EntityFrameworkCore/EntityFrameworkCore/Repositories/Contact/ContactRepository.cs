﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.EntityFrameworkCore;

namespace Contact.EntityFrameworkCore.Repositories.Contact
{
    public class ContactRepository : ContactRepositoryBase<Models.Contact, int>, IContactRepository
    {
        public ContactRepository(IDbContextProvider<ContactDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
