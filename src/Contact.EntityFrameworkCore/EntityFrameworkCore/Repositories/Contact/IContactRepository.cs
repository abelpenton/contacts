﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contact.EntityFrameworkCore.Repositories.Contact
{
    public interface IContactRepository: IRepository<Models.Contact>
    {
    }
}
