﻿using System;
using System.Collections.Generic;
using System.Text;
using Bogus;
namespace Contact.EntityFrameworkCore.Seed.Contact
{
    public class SeedContact
    {
        private readonly ContactDbContext _context;

        public SeedContact(ContactDbContext context)
        {
            _context = context;
        }

        public void Create(int count = 100)
        {
            var f = new Faker<Models.Contact>()
                .RuleFor(x => x.First_Name, y => y.Name.FirstName())
                .RuleFor(x => x.Last_Name, y => y.Name.LastName())
                .RuleFor(x => x.Profession, y => y.Person.Company.Name);


            var seeds = f.Generate(count);

            foreach (var item in seeds)
            {
                
                _context.Contacts.Add(item);
                _context.SaveChangesAsync();
            }
        }
    }
}
