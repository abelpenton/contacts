﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Contact.Configuration;
using Contact.Web;

namespace Contact.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class ContactDbContextFactory : IDesignTimeDbContextFactory<ContactDbContext>
    {
        public ContactDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ContactDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            ContactDbContextConfigurer.Configure(builder, configuration.GetConnectionString(ContactConsts.ConnectionStringName));

            return new ContactDbContext(builder.Options);
        }
    }
}
