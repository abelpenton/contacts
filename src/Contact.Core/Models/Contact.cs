﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contact.Models
{
    public class Contact: FullAuditedEntity
    {
        public string First_Name { get; set; }

        public string Last_Name { get; set; }

        public string Profession { get; set; }
    }
}
