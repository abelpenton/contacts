﻿using Abp.Authorization;
using Contact.Authorization.Roles;
using Contact.Authorization.Users;

namespace Contact.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
