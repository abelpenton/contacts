﻿namespace Contact
{
    public class ContactConsts
    {
        public const string LocalizationSourceName = "Contact";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
